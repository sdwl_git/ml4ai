const path = require('path')
const HtmlWebpackPlugin = require("html-webpack-plugin")
const MiniCssExtractPlugin = require("mini-css-extract-plugin")
const { webpackMode } = require('./env/webpack.config')

module.exports = {
    mode:webpackMode,
    entry: {
        app: path.join(__dirname, './client/client-entry.js')
    },
    output: {
        filename: 'client-entry.js',
        path: path.join(__dirname, './dist/public'),
        publicPath: "/public/"
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                use: {
                    loader: "babel-loader",
                    options: {
                        "presets": [
                            "env",
                            "react",
                            "es2015",
                            "stage-2",
                        ]
                    }
                },
                exclude: [
                    path.join(__dirname, "../node_modules")
                ],

            }, {
                test: /\.html$/,
                use: {
                    loader: "html-loader",
                    options: { minimize: false }
                }
            },
            {
                test: /\.(s)?css$/,
                exclude: /node_modules/,
                loader: [
                    MiniCssExtractPlugin.loader,
                    "css-loader",
                    "sass-loader"
                ]
            },
            {
                test: /\.(png|jpg|gif|woff|svg|eot|woff2|tff|svg)$/,
                use: 'url-loader?limit=8129',
                exclude: /node_modules/
            }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: path.resolve(__dirname, './public/index.html'),
            path: path.resolve(__dirname, "./dist"),
            chunksSortMode: 'none'
        }),
        new MiniCssExtractPlugin({
            filename: '[name].css',
            chunkFilename: '[id].css'
        })
    ]
}