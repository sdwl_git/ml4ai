import React from 'react'
import { debug } from '../env/config'
import Index from "../pages"

const routes = [{
    path: "/index",
    component: () => { if (debug) { console.log("路由：/index") } return <Index routePage="/assets/html/index.html" /> }
}, {
    path: "/ld",
    component: () => { if (debug) { console.log("路由：/ld") } return <Index routePage="/assets/mdfiles/largedata/largedata.md" /> }
}, {
    path: "/ds",
    component: () => { if (debug) { console.log("路由：/ds") } return <Index routePage="/assets/pdfs/datascience.pdf" /> }
}, {
    path: "/sparkQDA",
    component: () => { if (debug) { console.log("路由：/sparkQDA") } return <Index routePage="/assets/pdfs/SparkQuickAnlysis.pdf" /> }
}, {
    path: "/cls",
    component: () => { if (debug) { console.log("路由：/cls") } return <Index routePage="/assets/mdfiles/machinelearning/classification.md" /> }
}, {
    path: "/reg",
    component: () => { if (debug) { console.log("路由：/reg") } return <Index routePage="/assets/mdfiles/machinelearning/regression.md" /> }
}, {
    path: "/rc",
    component: () => { if (debug) { console.log("路由：/rc") } return <Index routePage="/assets/mdfiles/machinelearning/clustering.md" /> }
}, {
    path: "/nn",
    component: () => { if (debug) { console.log("路由：/nn") } return <Index routePage="/assets/mdfiles/deeplearning/nn.md" /> }
}, {
    path: "/bp",
    component: () => { if (debug) { console.log("路由：/bp") } return <Index routePage="/assets/mdfiles/deeplearning/bp.md" /> }
}, {
    path: "/tf",
    component: () => { if (debug) { console.log("路由：/tf") } return <Index routePage="/assets/mdfiles/deeplearning/tensorflow.md" /> }
}, {
    path: "/deeplearning",
    component: () => { if (debug) { console.log("路由：/deeplearning") } return <Index routePage="/assets/pdfs/deeplearning.pdf" /> }
}, {
    path: "/la",
    component: () => { if (debug) { console.log("路由：/la") } return <Index routePage="/assets/pdfs/linear.pdf" /> }
}, {
    path: "/ps",
    component: () => { if (debug) { console.log("路由：/ps") } return <Index routePage="/assets/pdfs/ProbabilityAndMathematicalStatistics.pdf" /> }
}, {
    path: "/i",
    component: () => { if (debug) { console.log("路由：/i") } return <Index routePage="/assets/mdfiles/math/calculus.md" /> }
}]

routes.default = {
    path: "/",
    component: () => { if (debug) { console.log("路由：/") } return <Index routePage="/assets/html/index.html" /> }
}

export { routes }