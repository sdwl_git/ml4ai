import React from 'react'
import { Menu } from 'antd'
import map from '../../../../ctl/mapper'
import { Link } from 'react-router-dom'
import NetProvider from '../../net/net'


class MenuComponent extends React.Component {

    constructor(prop) {
        super(prop)
        this.net = new NetProvider()
    }

    componentWillMount() {
        
    }


    componentDidMount() {

    }

    render() {
        return <Menu mode="horizontal">
            <Menu.Item key="/assets/html/index.html"><Link to="/index">首页</Link></Menu.Item>
            <Menu.SubMenu title="大数据">
                <Menu.Item key="/assets/mdfiles/deeplearning/largedata.md"><Link to="/ld">大数据</Link></Menu.Item>
                <Menu.Item key="/assets/pdfs/datascience.pdf"><Link to="/ds">数据科学</Link></Menu.Item>
                <Menu.SubMenu title="参考书籍">
                    <Menu.Item key="/assets/pdfs/SparkQuickAnlysis.pdf"><Link to="/sparkQDA">Spark快速大数据分析</Link></Menu.Item>
                </Menu.SubMenu>
            </Menu.SubMenu>
            <Menu.SubMenu title="人工智能">
                <Menu.SubMenu title="机器学习">
                    <Menu.Item key="/assets/mdfiles/machinelearning/classification.md"><Link to="/cls">分类</Link></Menu.Item>
                    <Menu.Item key="/assets/mdfiles/machinelearning/regression.md"><Link to="/reg">回归</Link></Menu.Item>
                    <Menu.Item key="/assets/mdfiles/machinelearning/clustering.md"><Link to="/rc">聚类</Link></Menu.Item>
                </Menu.SubMenu>

                <Menu.SubMenu title="深度学习">
                    <Menu.Item key="/assets/mdfiles/deeplearning/nn.md"><Link to="/nn">神经网络</Link></Menu.Item>
                    <Menu.Item key="/assets/mdfiles/deeplearning/bp.md"><Link to="/bp">BP神经网络</Link></Menu.Item>
                    <Menu.SubMenu title="工具箱">
                        <Menu.Item key="/assets/mdfiles/deeplearning/tensorflow.md"><Link to="/tf">Tensorflow</Link></Menu.Item>
                    </Menu.SubMenu>
                    <Menu.SubMenu title="深度学习教程">
                        <Menu.Item key="/assets/pdfs/deeplearning.pdf"><Link to="/deeplearning">深度学习理论</Link></Menu.Item>
                    </Menu.SubMenu>
                </Menu.SubMenu>
            </Menu.SubMenu>
            <Menu.SubMenu title="数学">
                <Menu.Item key="/assets/pdfs/linear.pdf"><Link to="/la">线性代数</Link></Menu.Item>
                <Menu.Item key="/assets/pdfs/ProbabilityAndMathematicalStatistics.pdf"><Link to="/ps">概率论数理统计</Link></Menu.Item>
                <Menu.Item key="/assets/mdfiles/math/calculus.md"><Link to="/i">微积分</Link></Menu.Item>
            </Menu.SubMenu>
        </Menu>
    }

}

export default map(MenuComponent)