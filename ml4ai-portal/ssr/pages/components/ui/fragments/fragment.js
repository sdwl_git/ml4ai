import React from 'react'
import NetProvider from '../../net/net'
import { debug } from '../../../../env/config'
import map from '../../../../ctl/mapper'

const Component = React.Component;

class Fragment extends Component {

    constructor(props) {
        super(props);
    }

    pull(url) {
        const net = new NetProvider()
        net.get(url, (res) => {
            let dataKey = `${url}`
            const data = {
            }
            data[dataKey] = res['data']
            this.props.dispatch({
                type: 'update',
                props: data
            })
            this.props.dispatch({
                type: "action",
                action: (state) => {
                    let newState = Object.assign({}, state)
                    if (newState.taskCount) {
                        newState.taskCount--
                    }
                    return newState
                }
            })
        }, (error) => {
            console.log('发生错误', error)
            let dataKey = `${url}`
            const data = {
            }
            data[dataKey] = ''
            this.props.dispatch({
                type: 'update',
                props: data
            })
            this.props.dispatch({
                type: "action",
                action: (state) => {
                    let newState = Object.assign({}, state)
                    if (newState.taskCount) {
                        newState.taskCount--
                    }
                    return newState
                }
            })
        }, this)
        if (debug) {
            console.log("加入url请求", url)
        }
        this.props.dispatch({
            type: "action",
            action: (state) => {
                let newState = Object.assign({}, state)
                if (newState.taskCount == undefined) {
                    newState.taskCount = 0
                }
                newState.taskCount++
                return newState
            }
        })
    }

    componentWillMount() {
        if (!this.props[this.props.url]) {
            if (debug) {
                console.log("拉取：" + this.props.url)
            }
            this.pull(this.props.url)
        }
    }

    componentDidMount() {
        if (!this.props[this.props.url]) {
            this.pull(this.props.url)
        }
    }

    componentWillReceiveProps(props) {
        if (!props[props.url]) {
            this.pull(props.url)
        }
    }

    render() {
        return <div dangerouslySetInnerHTML={
            {
                __html: this.props[this.props.url]
            }
        }></div >
    }

}

export default map(Fragment)