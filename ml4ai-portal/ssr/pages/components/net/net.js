import axios from 'axios'

import { port, debug } from '../../../env/config'

const getPrefix = () => {
    if (typeof window != 'undefined') {
        console.log("走浏览器请求")
        return ""
    } else {
        let prefix = `http://localhost:${port}/`
        if (debug) {
            console.log("走服务器请求")
            console.log(prefix)
        }
        return prefix
    }
}

export default class NetProvider {

    postJson(url, data, success, error, context) {
        let addPath = url.startsWith("http://") || url.startsWith("https://") ? "" : getPrefix()
        axios.post(addPath + url, data, {
            'Content-type': "application/json;charset=utf-8"
        }).then((response) => {
            if (success) {
                success.bind(context)
                success(response)
            }
        }).catch(errorRes => {
            if (error) {
                error.bind(context)
                error(errorRes)
            }
        })
    }

    post(url, data, success, error, context) {
        let addPath = url.startsWith("http://") || url.startsWith("https://") ? "" : getPrefix()
        axios.post(addPath + url, data, {}).then((response) => {
            if (success) {
                success.bind(context)
                success(response)
            }
        }).catch(errorRes => {
            if (error) {
                error.bind(context)
                error(errorRes)
            }
        })
    }

    get(url, success, error, context) {
        let addPath = url.startsWith("http://") || url.startsWith("https://") ? "" : getPrefix()
        axios.get(addPath + url).then((response) => {
            if (success) {
                success(response)
            }
        }).catch(errorRes => {
            if (error) {
                error(errorRes)
            }
        })
    }

}