## Tensorflow

　　tensorflow是google公司主导开发的一款用于深度学习的框架，它通过计算图的形式来表达在深度神经网络中计算的过程，其中包括众多的操作如：矩阵运算、张量运算、非线性、线性激活函数、卷积、池化、采样评价等。

# Tensorflow的安装

## Anaconda安装

　　网址：https://www.anaconda.com/

　　下载：https://www.anaconda.com/download/
　　
　　这里下载完成以后，python就已经安装好了。

## Tensorflow安装

　　1、打开 cmd

　　2、输入：pip install tensorflow （如果有支持的GPU，则安装：tensorflow-gpu）

　　3、等安装完毕

# 搭建一个模型

　　Tensorflow为我们提供了一套API，可以很方便的使用它来搭建神经网络，当然前提是你有神经网络方面的知识。Tensorflow是一个计算图工具，通过建立计算图来实现DNN。

　　限于内容太多，这里就给我写的一个例子：

　　网址：https://gitee.com/sleechengn/pbase/blob/master/MxClassification.py

# PyCharm安装

　　PyCharm是一个非常好的python集成开发环境，它支持输入后提示非常方便，PyCharm安装好以后，需要配置一些基本参数，具体参数可以搜索。