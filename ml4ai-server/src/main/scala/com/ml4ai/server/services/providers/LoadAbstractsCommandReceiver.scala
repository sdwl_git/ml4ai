package com.ml4ai.server.services.providers

import java.util

import com.google.common.collect.Lists
import com.google.gson.{Gson, GsonBuilder, JsonParser}
import com.ml4ai.server.services.{DbMapService, RabbitmqService, StreamSourceService, UserService}
import com.ml4ai.server.services.capability.{Command, CommandReceiver}
import com.ml4ai.elasticsearch.ElasticSearches
import javax.annotation.PostConstruct
import org.springframework.beans.factory.annotation.{Autowired, Value}
import org.springframework.stereotype.Service

import com.ml4ai.rabbitmq.RabbitMQ

import scala.collection.JavaConverters._

@Service
class LoadAbstractsCommandReceiver extends CommandReceiver {

  @Value("${elasticsearch.servers}") private val esServers: String = null
  @Value("${elasticsearch.cluster}") private val cluster: String = null
  @Autowired var rabbitmqService: RabbitmqService = null
  private var elasticsearch: ElasticSearches = null
  private var rabbitMQAgent: RabbitMQ = null
  private var googleJsonWrapperBean: Gson = null
  private var jsonParser: JsonParser = null

  @PostConstruct def init(): Unit = {
    elasticsearch = ElasticSearches.builder.esServers(esServers).esCluster(cluster).build
    rabbitMQAgent = rabbitmqService.getRabbitMQAgent
    jsonParser = new JsonParser
    val googleJsonWrapperBuilder = new GsonBuilder
    googleJsonWrapperBuilder.serializeNulls
    googleJsonWrapperBuilder.setDateFormat("yyyy-MM-dd")
    googleJsonWrapperBean = googleJsonWrapperBuilder.create
  }

  override def getSupport(): String = "loadAbstracts"

  override def execute(command: Command): Unit = {
    val start = command.get("start").toInt
    val size = command.get("size").toInt
    val parameter = googleJsonWrapperBean.fromJson(command.get("parameter"), classOf[util.Map[String, AnyRef]])
    val searchRequestBuilder = elasticsearch.getClient.prepareSearch("app-contents").setTypes("default")
    val searchResponse = searchRequestBuilder.setFrom(start).setSize(size).get
    val list = Lists.newArrayList(searchResponse.getHits.iterator()).asScala.map(hit => hit.getSourceAsMap()).asJava
    val response = new java.util.HashMap[String, AnyRef]
    response.put("code", "200")
    response.put("msg", "摘取成功")
    response.put("data", list)
    rabbitMQAgent.produceText("", command.getCallback(), googleJsonWrapperBean.toJson(response), false)
  }

}
