package com.ml4ai.server.services.providers

import com.ml4ai.server.consts.UserConstants
import com.ml4ai.server.services.{DbMapService, RabbitmqService, UserService}
import com.ml4ai.server.services.capability._
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import com.ml4ai.gson._

@Service
class LogoutReceiver extends CommandReceiver {

  @Autowired
  var dbMapService: DbMapService = null

  @Autowired
  var message: RabbitmqService = null

  @Autowired
  var userService: UserService = null

  override def execute(command: Command): Unit = {
    val token = command.get("token")
    val tokenStoreMap = dbMapService.generateLocalMapWrapper(UserConstants.TOKEN_USER_MAP)
    tokenStoreMap.remove(token)
    var msg = "{}"
    msg = msg.addString("code", "200")
    msg = msg.addString("message", "已注销")
    message.getRabbitMQAgent.produceText("", command.getCallback(), msg, false)
  }

  override def getSupport(): String = "logout"

}
