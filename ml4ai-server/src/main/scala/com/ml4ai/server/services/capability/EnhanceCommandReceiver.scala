package com.ml4ai.server.services.capability

import com.ml4ai.server.services.RabbitmqService
import org.springframework.beans.factory.annotation.Autowired

abstract class EnhanceCommandReceiver extends CommandReceiver {

  @Autowired
  val rabbitmqService: RabbitmqService = null

  override def execute(command: Command): Unit = {
    rabbitmqService.getRabbitMQAgent.produceText("", command.getCallback(), post(command), false)
  }

  def post(command: Command): String

}
