package com.ml4ai.server.user

import com.ml4ai.server.security.SecurityUtils
import com.ml4ai.server.services.UserService
import com.ml4ai.server.utils.RestUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.{RequestMapping, RestController}

@RestController
@RequestMapping(Array("/api/user"))
class UserResource {

  @Autowired
  var userService: UserService = null

  @RequestMapping(Array("/getMenus"))
  def getMenus() = {
    val userDTO = SecurityUtils.getCurrentUser
    RestUtil.success(userService.findByUserId(userDTO.getId))
  }

}
