package com.ml4ai.server.services;

import com.ml4ai.server.domain.Authority;
import com.ml4ai.server.dto.AuthorityDTO;
import com.ml4ai.server.services.base.BaseService;

/**
 * Created by leecheng on 2018/9/24.
 */
public interface AuthorityService extends BaseService<Authority, AuthorityDTO> {
}
