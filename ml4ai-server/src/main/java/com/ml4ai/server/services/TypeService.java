package com.ml4ai.server.services;

import com.ml4ai.server.domain.Type;
import com.ml4ai.server.dto.TypeDTO;
import com.ml4ai.server.services.base.BaseService;

/**
 * Created by uesr on 2018/9/12.
 */
public interface TypeService extends BaseService<Type, TypeDTO> {
}
