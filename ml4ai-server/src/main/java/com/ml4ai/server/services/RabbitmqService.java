package com.ml4ai.server.services;

import com.ml4ai.rabbitmq.RabbitMQ;

public interface RabbitmqService {

    RabbitMQ getRabbitMQAgent();

}
