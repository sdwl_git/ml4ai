package com.ml4ai.server.services;

import com.ml4ai.server.domain.Menu;
import com.ml4ai.server.dto.MenuDTO;
import com.ml4ai.server.services.base.BaseService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by uesr on 2018/9/2.
 */
@Service
public interface MenuService extends BaseService<Menu, MenuDTO> {

    List<MenuDTO> getAllMenus();

}
