package com.ml4ai.server.services.base;

import com.ml4ai.server.domain.base.BaseAuditEntity;
import com.ml4ai.server.dto.base.BaseAuditDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Created by 李程 on 2018/9/2.
 */
public interface BaseService<T extends BaseAuditEntity, K extends BaseAuditDTO> {

    K findById(Long id);

    K save(K k);

    Boolean delete(Long id);

    List<K> query(Object k);

    Page<K> queryPage(Object k, Pageable pageable);

    Long count(Object k);
}
