package com.ml4ai.server.dto;

import com.ml4ai.server.domain.base.IntraCityDistributionStatus;
import com.ml4ai.server.utils.annotation.QueryColumn;
import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;

/**
 * Created by uesr on 2018/9/12.
 */
@Data
public class IntraCityDistributionTaskDTO extends TaskDTO {

    @NotNull
    private Double sourceX;

    @NotNull
    private Double sourceY;

    @NotEmpty
    private String sourceAddressInfo;

    @NotEmpty
    private String sourceAddress;

    @NotEmpty
    private String sourceLink;

    @NotEmpty
    private String sourceTelephone;

    @NotNull
    private Double destinationX;

    @NotNull
    private Double destinationY;

    @NotEmpty
    private String destinationAddressInfo;

    @NotEmpty
    private String destinationAddress;

    @NotEmpty
    @QueryColumn
    private String destinationLink;

    @NotEmpty
    @QueryColumn
    private String destinationTelephone;

    @NotEmpty
    private String commodityName;

    @QueryColumn
    private IntraCityDistributionStatus intraCityDistributionStatus;

    private Double distance;

}
