package com.ml4ai.spark

import org.apache.spark.{SparkConf, SparkContext}

object Boot {

  def main(args: Array[String]): Unit = {
    val conf = new SparkConf
    val sc = new SparkContext(conf)
    println(Thread.currentThread().getContextClassLoader)
    sc.makeRDD(1 to 100).map(x => x * x / 2).collect.foreach(println)
    System.exit(0)
  }

}