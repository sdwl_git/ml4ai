import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { AppHeaderComponent } from './app-header/app-header';
import { TaskListComponent } from './task-list/task-list';
import { InfoListComponent } from './info-list/info-list';
@NgModule({
	declarations: [
		AppHeaderComponent,
		TaskListComponent,
		InfoListComponent
	],
	imports: [
		IonicModule
	],
	entryComponents: [
	],
	exports: [
		AppHeaderComponent,
		TaskListComponent,
		InfoListComponent
	]
})
export class ComponentsModule { }
