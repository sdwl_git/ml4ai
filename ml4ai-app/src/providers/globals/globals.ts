import { Injectable } from '@angular/core';
import { HomePage } from '../../pages/home/home';
import { NavController } from 'ionic-angular';
import { ExtendsApp } from '../../app/app.component';
import { StorageProvider } from '../storage/storage';

/*
  Generated class for the GlobalsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class GlobalsProvider {

  public urls = {
    login: "http://localhost:8080/api/login",
    menuGet: "http://localhost:8080/api/menu/get",
    intraCityDistributionQuery: "http://localhost:8080/api/business/task/queryIntraCityDistributionTask",
    getIntraCityDistribution: "http://localhost:8080/api/business/task/getIntraCityDistribution",
    myTask: "http://localhost:8080/api/business/task/queryMyTask",
    enums: "http://localhost:8080/api/public/enums",
    geoInfo: "http://localhost:8080/api/map/geo/info",
    saveIntraCityDistribution: "http://localhost:8080/api/business/task/saveIntraCityDistribution",
    newIntraCityDistribution: "http://localhost:8080/api/business/task/createIntraCityDistribution",
    deleteIntraCityDistribution: "http://localhost:8080/api/business/task/deleteIntraCityDistribution",
    publishIntraCityDistribution: "http://localhost:8080/api/business/task/publishIntraCityDistribution",
    mapgetdistance: "http://localhost:8080/api/map/getDistance"
  };

  public root: ExtendsApp;
  public authToken: string;
  public navControl: NavController;
  public messageHandlers = {
  };
  public ws: WebSocket;

  constructor(public store: StorageProvider) {
    try {
      let authToken = store.load("X-AUTH-TOKEN");
      if (authToken) {
        this.authToken = authToken;
      }
    } catch (e) {
      console.debug(e);
    }
    this.initWebSocket(this);
  }

  initWebSocket(me: GlobalsProvider) {
    me.ws = new WebSocket("wss://192.168.0.109/api/stream/socket");
    me.ws.onopen = () => {

    };
    me.ws.onmessage = (msg) => {
      for (let key in me.messageHandlers) {
        let handler = me.messageHandlers[key];
        handler(msg.data);
      }
    };
    me.ws.onerror = (err) => {
      me.initWebSocket(me);
    };
    me.ws.onclose = () => {
      me.initWebSocket(me);
    };
  }

  setWebSocketHandler(key: string, handler: any) {
    this.messageHandlers[key] = handler;
  }

}
