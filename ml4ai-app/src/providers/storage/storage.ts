import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the StorageProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class StorageProvider {

  constructor() {

  }

  store(key: string, val: any) {
    localStorage.setItem(key, JSON.stringify(val));
  }

  load(key: string) {
    let origin = localStorage.getItem(key);
    if (origin) {
      return JSON.parse(origin);
    } else {
      return undefined;
    }
  }

}
