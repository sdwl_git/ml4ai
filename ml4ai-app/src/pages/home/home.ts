import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { NetProvider } from '../../providers/net/net';
import { GlobalsProvider } from '../../providers/globals/globals';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  /**
  public picMark: string = '';
  public content: string = '';
  public headers: Headers = new Headers({});
  */

  public debug = false;
  public list: string[] = [];

  constructor(public navCtrl: NavController, private camera: Camera, private net: NetProvider, private globals: GlobalsProvider) {
    globals.navControl = navCtrl;
  }

  ionViewDidLoad() {
    this.list.push("方法[ionViewDidLoad]执行");
  }

  ionViewWillEnter() {
    this.list.push("方法[ionViewWillEnter]执行");
  }

  ionViewDidEnter() {
    this.list.push("方法[ionViewDidEnter]执行");
  }

  ionViewWillLeave() {
    this.list.push("方法[ionViewWillLeave]执行");
  }

  ionViewDidLeave() {
    this.list.push("方法[ionViewDidLeave]执行");
  }

  ionViewWillUnload() {
    this.list.push("方法[ionViewWillUnload]执行");
  }

  click() {
    this.debug = false;
  }

  //照相功能
  /**
  public capture() {
    const options: CameraOptions = {
      quality: 100,
      sourceType: this.camera.PictureSourceType.CAMERA,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }
    this.camera.getPicture(options).then((imageData) => {
      let base64Image = 'data:image/jpeg;base64,' + imageData;
      this.picMark = '<img (click)="capture()" src="' + base64Image + '"></img>';
    }, (err) => {
      console.log('获取图片失败');
    });
  }
  */

  //请求数据
  /**
  public loadData() {
    this.http.post('http://www.lietou.com/', JSON.stringify({}), { headers: this.headers }).subscribe((data) => {
      console.log(data);
      this.content = data['_body'];
      alert(data);
    }, (err) => {
      console.log(err);
      alert(err);
    });
  }
  */

}
