package com.ml4ai.core;

import java.lang.reflect.Method;
import java.net.URL;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class IsolationExecution {

    private URL[] sources;
    private IsolationUrlClassLoader isoloader;

    public IsolationExecution(URL... sources) {
        this.sources = sources;
        isoloader = new IsolationUrlClassLoader(findJavaLoader(), this.sources);
    }

    public IsolationExecution(ClassLoader subLoader, URL... sources) {
        this.sources = sources;
        isoloader = new IsolationUrlClassLoader(findJavaLoader(), subLoader, this.sources);
    }

    /**
     * 隔离执行静态方法
     *
     * @param className
     * @param methodName
     * @param args
     * @param <T>
     * @return
     */
    public <T> T execute(String className, String methodName, Object... args) throws Exception {
        BlockingQueue<Object[]> takker = new ArrayBlockingQueue<>(1);
        Thread executionThread = new Thread(() -> {
            try {
                ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
                Class clz = classLoader.loadClass(className);
                Method[] methods = clz.getMethods();
                Object[] values = new Object[1];
                for (Method method : methods) {
                    if (method.getName().equals(methodName) && (((args == null || args.length == 0) && method.getParameterCount() == 0) || (args != null && args.length > 0 && args.length == method.getParameterCount()))) {
                        try {
                            values[0] = method.invoke(null, args);
                        } catch (Exception e) {
                            values[0] = e;
                        }
                    }
                }
                takker.put(values);
            } catch (Exception e) {
                try {
                    takker.put(new Object[]{e});
                } catch (Exception ex) {
                    System.err.println("发生致命错误!");
                    System.exit(0);
                }
            }
        });
        executionThread.setContextClassLoader(isoloader);
        executionThread.start();
        Object[] v = takker.take();
        if (v[0] != null && v[0] instanceof Exception) {
            throw (Exception) v[0];
        } else {
            return (T) v[0];
        }
    }

    public ClassLoader getLoader() {
        return isoloader;
    }

    public void execute(Runnable runnable) {
        Thread thread = new Thread(runnable);
        thread.setContextClassLoader(isoloader);
        thread.start();
    }

    public static ClassLoader findJavaLoader() {
        ClassLoader cl = IsolationUrlClassLoader.class.getClassLoader();
        while (cl.getParent() != null) {
            cl = cl.getParent();
        }
        return cl;
    }

}
