import React from 'react'
import { AjaxListTable, ListTable, DataForm, FormWrapper } from '../common/display';
import { West, North, South, East, Center, Vertical, Horizontal } from '../common/layout';
import { ajax, api } from '../../config.service';
import { Modal, Form } from 'antd';
import { Button } from 'antd/lib/radio';

class AppContent extends React.Component {
    constructor(prop) {
        super(prop)
        this.state = {
            visible: false
        };
    }

    render() {


        let me = this;
        let start = 0;
        let limit = 12
        return (<div className="width100height100">
            <Vertical>
                <North height={120}>
                    <div className="width100height100"></div>
                </North>
                <Center>
                    <div className="width100height100">
                        <AjaxListTable url={`${api.queryContent}?offset=${start}&limit=${limit}`} translate={{ name: "名称", date: "日期", introduce: "简介" }} exclude={["class", "content", "createUser"]} requestData={{}} path="data"
                            operators={[
                                {
                                    title: "编缉",
                                    fun: (data) => {
                                        me.setState({ visible: true, formData: data });
                                    }
                                }, {
                                    title: "删除",
                                    url: `${api.delContent}`,
                                    callback: (response) => {
                                        me.setState({ visible: false, formData:{} });
                                    }
                                }
                            ]} >
                        </AjaxListTable>
                        <div className="height10"></div>
                        <div className="margin-left10">
                            <Button onClick={() => { me.setState({ visible: true, formData: {} }); }}>添加</Button>
                        </div>
                    </div>
                </Center>
            </Vertical>
            <Modal okText="确定" onOk={
                () => {
                    let formData = me.formWrapper.getFieldValues();
                    let origin = this.state.formData ? this.state.formData : {};
                    formData["id"] = origin["id"];
                    ajax.postJson(api.saveContent, formData, (response) => {
                        me.setState({ visible: false, formData: {} });
                    }, (response) => {
                    });
                }
            }
                cancelText="取消"
                onCancel={
                    () => {
                        me.setState({ visible: false, formData: {} });
                    }
                }
                visible={this.state.visible} >
                <FormWrapper createCallback={(wrapper) => { me.formWrapper = wrapper; }} items={[
                    {
                        "name": "名称",
                        "key": "name"
                    },
                    {
                        "name": "日期",
                        "key": "date"
                    },
                    {
                        "name": "简介",
                        "key": "introduce"
                    }
                ]} values={this.state.formData ? this.state.formData : {}} />
            </Modal>
        </div>)
    }
}

export { AppContent }